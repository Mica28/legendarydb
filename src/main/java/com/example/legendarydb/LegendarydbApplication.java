package com.example.legendarydb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LegendarydbApplication {

    public static void main(String[] args) {
        SpringApplication.run(LegendarydbApplication.class, args);
        System.out.println("HELLO WORLD !");
    }

}
